Positive Control
Pulled 13 cdiff genomes (completed)
Ran Sample_CD01240_R1 and R2

1. Diamond positive control

a. R1 reads
Total time = 438.82s
Reported 11469329 pairwise alignments, 11523524 HSSPs.
1203887 queries aligned.

b. R2 reads
Total time = 434.79s
Reported 10695791 pairwise alignments, 10747356 HSSPs.
1126269 queries aligned.


2. Lambda positive control
a. R1 reads
Number of valid hits:                           22683184
Number of Queries with at least one valid hit:   1711489

b. R2 reads
Number of valid hits:                           21185251
Number of Queries with at least one valid hit:   1607476


3. PAUDA positive control

a. Writing index / 'database' files
Returning from initFromVector
Wrote 5281584 bytes to primary EBWT file: ./index/ref.rev.1.bt2
Wrote 683160 bytes to secondary EBWT file: ./index/ref.rev.2.bt2

b. R1 reads
Writing output file:  all_complete_Gb_bac_protein_cdiff_pauda1.blastx
10% 20% 30% 40% 50% 60% 70% 80% 90% 100% (484.3s)
Total DNA reads in SAM: 516721
Total DNA reads out:    500462
Total matches in:       1109956
Total matches out:     4096471
Time: 485s

c. R2 reads
Writing output file:  all_complete_Gb_bac_protein_cdiff_pauda2.blastx
10% 20% 30% 40% 50% 60% 70% 80% 90% 100% (405.2s)
Total DNA reads in SAM: 480365
Total DNA reads out:    464655
Total matches in:       1029761
Total matches out:     3795888
Time: 406s








