# MOCAT2 testing
# July 25, 2016

# 1. Pulled CD01240 sample R1 and R2 fastq files into this project folder
# created my.samples that contain the file names

# 2. Load module for MOCAT2
module load mocat2

# 3. submitted to Minerva by itself, this first command preprocesses reads (read trim filter), takes about 7 hours
MOCAT.pl -sf my.samples -rtf

# 4. assembly 
MOCAT.pl -sf my.samples -a -r reads.processed

# 5. not sure what this step is
MOCAT.pl -sf my.samples -gp assembly

