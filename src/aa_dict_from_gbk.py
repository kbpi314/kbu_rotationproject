import sys

gbk_fn = sys.argv[1]  # protein database
output_fn = sys.argv[2] # m8 output file

def build_aa_dict_from_gbk (gbkfile):
    from Bio import SeqIO
    aa_dict = {}
    gid = gbkfile.split(".")[0]
    for seq_record in SeqIO.parse(gbkfile, "genbank"):
        for feat in seq_record.features:
            if "translation" in feat.qualifiers:
                locus_tag = feat.qualifiers['locus_tag']
                if "gene" in feat.qualifiers:
                    gene = feat.qualifiers['gene']
                else:
                    gene = ["UNKNOWN"]
                locus_tag = locus_tag[0]
                gene = gene[0]
                new_name = gid + "__" + locus_tag + "__" + gene
                aa_dict[new_name] = feat.qualifiers['translation'][0]
    return aa_dict

def convert_aa_dict_to_fasta (aa_dict, out=sys.stdout):
    for aa_id, aa_seq in aa_dict.items():
        out.write(">%s\n%s\n" %(aa_id, aa_seq))

aa_dict = build_aa_dict_from_gbk(gbk_fn)
convert_aa_dict_to_fasta(aa_dict, open(output_fn + '.faa', 'w'))


