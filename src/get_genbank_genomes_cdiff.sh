# get_genbank_genomes_cdiff.sh
# parse genbank for a list of complete genomes and create database from them

#1. Get the list of assemblies:
FILE=assembly_summary.txt
if [ -f $FILE ]; then echo 'File $FILE found'; else wget ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/assembly_summary.txt; fi

#2. Parse the addresses of complete cdiff genomes
FILE2=assembly_summary_cdiff.txt
if [ -f $FILE2 ];
then echo 'File $FILE2 found';
else
grep "difficile\|^#" assembly_summary.txt | grep "Complete Genome\|^#" > assembly_summary_cdiff.txt;
fi

#3. Write to summary file
FILE3=assembly_summary_addresses_complete_genomes_cdiff.txt
if [ -f $FILE3 ];
then echo 'File $FILE3 found';
else
awk -F '\t' '{if($12=="Complete Genome") print $20}' assembly_summary_cdiff.txt > assembly_summary_addresses_complete_genomes_cdiff.txt;
fi

#3. Make a dir for data
mkdir GbBacGenomesCdiff

#4. Fetch data
for next in $(cat assembly_summary_addresses_complete_genomes_cdiff.txt); do name=$(basename $next); wget -P GbBacGenomesCdiff "$next"/${name}_genomic.fna.gz; done

#5. Extract data
gunzip GbBacGenomesCdiff/*.gz

#6. Concatenate data
cat GbBacGenomesCdiff/*.fna > all_complete_Gb_bac_genomes_cdiff.fasta
~                                                                       
