import pysam
import sys
import matplotlib
matplotlib.use('Agg')

from collections import Counter
fn = sys.argv[1]
bin = int(sys.argv[2])

samfile = pysam.AlignmentFile(fn, "rb")


ref_cov = Counter()

for i in range (len(samfile.references)):
    ref = samfile.references[i]
    reflen = samfile.lengths[i]
    
    for j in range (0, reflen, bin):
        c = samfile.count(reference=ref, start=j, end=j+bin)
        ref_cov[j] = c

samfile.close()

import matplotlib.pyplot as plt
import seaborn as sns
itemset = ref_cov.items()
itemset.sort ()
sns.barplot(zip(*itemset)[0], zip(*itemset)[1])
plt.xlabel('Position')
plt.xticks(rotation=270)
plt.ylabel('Frequency')
plt.title('Read position in genome')

plt.savefig(fn + '.pdf', bbox_inches='tight')
# plt.show()
