from collections import Counter
import sys

fn = sys.argv[1]

species_set = set()
read_ids = {}
read_lines = {}
# read data to table
with open (fn) as f:
    for l in f:
        ll = l.strip().split()
        read_id, species = ll[0], ll[-1]
        species = species.split("/")[-1].split("_")[0]
        read_ids.setdefault(read_id, Counter())[species] += 1
        species_set.add(species)
        
# print header
print "V1",
species_list = list(species_set)
print " ".join(species_list),
print "diff_strains"
# print data to out table
for read in read_ids:
    print read,
    read_data = read_ids[read]
    counter = 0
    for species in species_list:
        # only count a genome as a "binary" indicator
        if read_data[species] > 0:
            counter += 1
        print read_data[species],
    print counter



