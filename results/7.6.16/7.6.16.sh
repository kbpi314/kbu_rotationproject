# July 6th, 2016

# /sc/orga/work/buk02/kbu_rotationproject/src/convertDiamondReadSpeciesMat.py 


SAMPLES=/sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/*
for s in $SAMPLES
do
f=$s/*.fastq
reads=($f)
name=$(basename $s)

# Read 1
# blast sample reads against database (less than 10 minutes)
/sc/orga/work/buk02/thirdparty/diamond blastx -d proteindb_cdiff -q ${reads[0]} -a ${name}.R1.diamond
# convert from faa to viewable form (less than 1 min)
/sc/orga/work/buk02/thirdparty/diamond view -a ${name}.R1.diamond.daa -o ${name}.R1.diamond.m8

line_no=$(awk '{x++} END {print x}' ${reads[0]})
read_no=`expr $line_no / 2` 
alpha=0.0005
sig=`echo "$alpha / $read_no" | bc -l`
             
awk -v sig=$sig '{if ($11 < sig) print $0}' ${name}.R1.diamond.m8 > ${name}.R1.diamond.m8.filt.txt

python /sc/orga/work/buk02/kbu_rotationproject/src/quick_join_genome_protein.py final_genbank_CDiff_proteins.fasta ${name}.R1.diamond.m8.filt.txt > ${name}.R1.diamond.m8.filt.wgenome.txt

python /sc/orga/work/buk02/kbu_rotationproject/src/convertDiamondReadSpeciesMat.py ${name}.R1.diamond.m8.filt.wgenome.txt > ${name}.R1.diamond.final.txt

# Read 2
# blast sample reads against database (less than 10 minutes)
/sc/orga/work/buk02/thirdparty/diamond blastx -d proteindb_cdiff -q ${reads[1]} -a ${name}.R2.diamond
# convert from faa to viewable form (less than 1 min)
/sc/orga/work/buk02/thirdparty/diamond view -a ${name}.R2.diamond.daa -o ${name}.R2.diamond.m8
line_no=$(awk '{x++} END {print x}' ${reads[1]})
read_no=`expr $line_no / 2` 
alpha=0.0005
sig=`echo "$alpha / $read_no" | bc -l`
             
awk -v sig=$sig '{if ($11 < sig) print $0}' ${name}.R2.diamond.m8 > ${name}.R2.diamond.m8.filt.txt 
python /sc/orga/work/buk02/kbu_rotationproject/src/quick_join_genome_protein.py final_genbank_CDiff_proteins.fasta ${name}.R2.diamond.m8.filt.txt > ${name}.R2.diamond.m8.filt.wgenome.txt

python /sc/orga/work/buk02/kbu_rotationproject/src/convertDiamondReadSpeciesMat.py ${name}.R2.diamond.m8.filt.wgenome.txt > ${name}.R2.diamond.final.txt
done

