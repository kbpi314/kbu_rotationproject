# June 21 2016
# acquire topological distribution of matching reads

# build databases, testing all .sh (done in login1)

/sc/orga/work/buk02/kbu_rotationproject/src/get_genbank_genomes.sh # <4 hr
/sc/orga/work/buk02/kbu_rotationproject/src/get_genbank_proteins.sh # <4 hr
/sc/orga/work/buk02/kbu_rotationproject/src/get_genbank_genomes_cdiff.sh # <5 min
/sc/orga/work/buk02/kbu_rotationproject/src/get_genbank_proteins_cdiff.sh # <5 min

# run diamond against R1 of Sample CD01240 (switch to interactive1)
# Create protein database from (8 protein sets, 1s)
/sc/orga/work/buk02/Thirdparty/diamond makedb --in all_complete_Gb_bac_proteins_cdiff.fasta -d proteindb_cdiff

# blast sample reads against database (less than 10 minutes)
/sc/orga/work/buk02/Thirdparty/diamond blastx -d proteindb_cdiff -q /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/CD01240_S11_L008_R1_001.fna -a Sample_CD01240_R1_cdiff_diamond

# convert from faa to viewable form (less than 1 min)
/sc/orga/work/buk02/Thirdparty/diamond view -a Sample_CD01240_R1_cdiff_diamond.daa -o Sample_CD01240_R1_cdiff_diamond.m8 

