# README #

Kevin Bu summer 2016 rotation project.

### What is this repository for? ###

V1.0
Metagenomics
Haplotype phasing

### Raw Data
# /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240
# Sample_CD01240  Sample_CD01252  Sample_CD01331  Sample_CD01382  Sample_CD01404                Sample_RC00284
# Sample_CD01244  Sample_CD01287  Sample_CD01351  Sample_CD01389  Sample_Control_solid_CD01374


### File directory structure

/sc/orga/scratch/buk02
6.8.16: all samples against cdiff db
6.9.16: spades assemblies
6.13.16: CD01240 against full db

/sc/orga/work/buk02/kbu_rotationproject/results



### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
