# June 28, 2016
# MOCAT2 testing

# 1. Pulled CD01240 sample R1 and R2 fastq files into this project folder
# created my.samples that contain the file names

# 2. Load module for MOCAT2
module load mocat2

# 3. submitted to Minerva by itself, this first command preprocesses reads (read trim filter), takes about 7 hours
MOCAT.pl -sf my.samples -rtf 

# 4. assembly 
MOCAT.pl -sf my.samples -a # -r reads.processed









# Get N50, mean, max
module load smrtpipe

CONTIGS=/sc/orga/scratch/buk02/6.9.16/Sample_Contigs/*
for c in $CONTIGS
do
# name=$(basename $c)
getN50 $c
fastalength $c | sort -nk 1 | head
fastalength $c | sort -nk 1 | tail
done


