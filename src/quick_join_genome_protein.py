# Takes a .fasta/.faa protein database file (serves as dictionary) and a blast result file in .m8 form
# and attaches the genome to the protein name in the .m8 file

import os

import sys

p_fn = sys.argv[1]  # protein database
m8_fn = sys.argv[2] # m8 output file
 
p2g = {}
with open (p_fn) as f:
    for l in f:
        if l[0] == ">":
            ll = l.strip().split("[")
            first, second = ll[0], ll[-1]
            protein = first.split(" ")[0][1:]
            second = second.strip().split("]")[0]
            genome = "_".join(second.split(" "))
            p2g[protein] = genome

with open (m8_fn) as f:
    for l in f:
        ll = l.strip().split()
        protein = ll[1]
        ll.append(p2g[protein])
        print "\t".join(ll)



