# June 6 2016

# Create positive control database of cdiff genomes, must do this in login1
# runs from the pwd /sc/orga/work/buk02/kbu_rotationproject/results/6.6.16
../../src/get_genbank_protein_cdiff.sh

# DIAMOND
# Create protein database from
../../../Thirdparty/diamond makedb --in all_complete_Gb_bac_protein_cdiff.fasta -d proteindb_cdiff

# blastx fasta against protein db, do this in interactive1
/sc/orga/work/buk02/Thirdparty/diamond blastx -d proteindb_cdiff -q /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/CD01240_S11_L008_R1_001.fna -a matchesproteinR1_cdiff
          
/sc/orga/work/buk02/Thirdparty/diamond blastx -d proteindb_cdiff -q /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/CD01240_S11_L008_R2_001.fna -a matchesproteinR2_cdiff

# convert from faa to viewable form
/sc/orga/work/buk02/Thirdparty/diamond view -a matchesproteinR1_cdiff.daa -o matchesproteinR1_cdiff.m8

/sc/orga/work/buk02/Thirdparty/diamond view -a matchesproteinR2_cdiff.daa -o matchesproteinR2_cdiff.m8



# LAMBDA
# build database
../../../Thirdparty/lambda-0.9.4-Linux-x86_64_sse4/bin/lambda_indexer -d all_complete_Gb_bac_protein_cdiff.fasta

# run queries
../../../Thirdparty/lambda-0.9.4-Linux-x86_64_sse4/bin/lambda -q /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/CD01240_S11_L008_R1_001.fna -d all_complete_Gb_bac_protein_cdiff.fasta

mv output.m8 output1.m8
# result in output1.m8

../../../Thirdparty/lambda-0.9.4-Linux-x86_64_sse4/bin/lambda -q /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/CD01240_S11_L008_R2_001.fna -d all_complete_Gb_bac_protein_cdiff.fasta

mv output.m8 output2.m8
# result in output2.m8

# PAUDA
# relies on bowtie2
module load bowtie2

# create database
env PAUDA_INDEX_DIR=./index ../../../Thirdparty/pauda-1.0.1/bin/pauda-build all_complete_Gb_bac_protein_cdiff.fasta

# run queries
../../../Thirdparty/pauda-1.0.1/bin/pauda-run /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/CD01240_S11_L008_R1_001.fna all_complete_Gb_bac_protein_cdiff_pauda1.blastx ./index
# result in all_complete_Gb_bac_protein_cdiff_pauda1.blastx

../../../Thirdparty/pauda-1.0.1/bin/pauda-run /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/CD01240_S11_L008_R2_001.fna all_complete_Gb_bac_protein_cdiff_pauda2.blastx ./index
# result in all_complete_Gb_bac_protein_cdiff_pauda2.blastx


# Testing e-values
#/sc/orga/work/buk02/Thirdparty/diamond makedb --in tprotein.fasta -d tproteindb
/sc/orga/work/buk02/Thirdparty/diamond makedb --in tprotein2.fasta -d tproteindb2

TEST=/sc/orga/work/buk02/kbu_rotationproject/results/6.6.16/test*.fasta
for t in $TEST
do
v=$(basename $t)
#/sc/orga/work/buk02/Thirdparty/diamond blastx -d proteindb_cdiff -q $t -a ${v}.cdiff_diamond_matches
#/sc/orga/work/buk02/Thirdparty/diamond view -a ${v}.cdiff_diamond_matches.daa -o ${v}.cdiff_diamond_matches.m8

#/sc/orga/work/buk02/Thirdparty/diamond blastx -d tproteindb -q $t -a ${v}.t_diamond_matches
#/sc/orga/work/buk02/Thirdparty/diamond view -a ${v}.t_diamond_matches.daa -o ${v}.t_diamond_matches.m8

/sc/orga/work/buk02/Thirdparty/diamond blastx -d tproteindb2 -q $t -a ${v}.t2_diamond_matches
/sc/orga/work/buk02/Thirdparty/diamond view -a ${v}.t2_diamond_matches.daa -o ${v}.t2_diamond_matches.m8

done


