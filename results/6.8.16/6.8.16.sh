# June 8 2016
 
# coding with Ali (ran while pwd was 6.6.16.)
module load python py_packages
python quick_join_genome_protein.py all_complete_Gb_bac_protein_cdiff.fasta matchesproteinR1_cdiff.m8.filt.txt > matchesproteinR1_cdiff.m8.filt.wgenome.txt

head matchesproteinR1_cdiff.m8.filt.wgenome.txt


# spade assembly done in scratch, 6.9.16 (should take a week)
export PATH=/sc/orga/work/buk02/Thirdparty/SPAdes-3.8.0-Linux/bin/:$PATH

SAMPLES=/sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/*
for s in $SAMPLES
do
f=$s/*.fastq
reads=($f)
name=$(basename $s)
metaspades.py -1 ${reads[0]} -2 ${reads[1]} --meta -t 16 -o ${name}.spades_assembly  
done

# test spades done in work/buk02
metaspades.py -1 /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/CD01240_S11_L008_R1_001.fastq -2 /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/CD01240_S11_L008_R2_001.fastq --meta -t 16 -o CD01240_spades_test

# 1. all samples through cdiff database
# /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240
# Sample_CD01240  Sample_CD01252  Sample_CD01331  Sample_CD01382  Sample_CD01404                Sample_RC00284
# Sample_CD01244  Sample_CD01287  Sample_CD01351  Sample_CD01389  Sample_Control_solid_CD01374

# example call
metaspades.py -1 /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_RC00284/RC00284_S4_L008_R1_001.fastq -2 /sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_RC00284/RC00284_S4_L008_R2_001.fastq --meta -t 16 -o Sample_RC00284.spades_assembly

# 1a. Diamond

# copied files over from 6.6.16 into scratch (ran while pwd was 6.6.16)
cp proteindb_cdiff.dmnd /sc/orga/scratch/buk02/6.8.16

# run all samples
SAMPLES=/sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/*
for s in $SAMPLES
do
f=$s/*.fastq
for r in $f
do
# blastx fasta against protein db, do this in interactive1
v=$(basename $r)
/sc/orga/work/buk02/Thirdparty/diamond blastx -d proteindb_cdiff -q $r -a ${v}.cdiff_diamond_matches
/sc/orga/work/buk02/Thirdparty/diamond view -a ${v}.cdiff_diamond_matches.daa -o ${v}.cdiff_diamond_matches.m8
done
done

# 1b. Lambda

# build database
/sc/orga/work/buk02/Thirdparty/lambda-0.9.4-Linux-x86_64_sse4/bin/lambda_indexer -d all_complete_Gb_bac_protein_cdiff.fasta

# run queries
SAMPLES=/sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/*
for s in $SAMPLES
do
f=$s/*.fastq
for r in $f
do
# blastx fasta against protein db, do this in interactive1
v=$(basename $r)
/sc/orga/work/buk02/Thirdparty/lambda-0.9.4-Linux-x86_64_sse4/bin/lambda -q $r -d all_complete_Gb_bac_protein_cdiff.fasta
mv output.m8 ${v}.cdiff_lambda_matches.m8
done
done

# 1c. PAUDA

#relies on bowtie2
module load bowtie2

# create database
# DO NOT mkdir index
env PAUDA_INDEX_DIR=./index /sc/orga/work/buk02/Thirdparty/pauda-1.0.1/bin/pauda-build all_complete_Gb_bac_protein_cdiff.fasta

# run queries
SAMPLES=/sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/*
for s in $SAMPLES
do
f=$s/*.fastq
for r in $f
do
# blastx fasta against protein db, do this in interactive1
v=$(basename $r)
/sc/orga/work/buk02/Thirdparty/pauda-1.0.1/bin/pauda-run $r ${v}.cdiff_pauda_matches.blastx ./index
# result in all_complete_Gb_bac_protein_cdiff_pauda1.blastx
done
done



# 2. one sample and its contigs through giant database
# done in /sc/orga/scratch/buk02/6.13.16

# 2a. Diamond

#construct all genome protein database ~ 1hr
/sc/orga/work/buk02/kbu_rotationproject/src/get_genbank_protein.sh

# Create protein database from ~ 4 min
/sc/orga/work/buk02/Thirdparty/diamond makedb --in all_complete_Gb_bac_protein.fasta -d complete_proteindb

SINGLE=/sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/*fastq
for r in $SINGLE
do
# blastx fasta against protein db, do this in interactive1
v=$(basename $r)
/sc/orga/work/buk02/Thirdparty/diamond blastx -d complete_proteindb -q $r -a ${v}.diamond_matches
/sc/orga/work/buk02/Thirdparty/diamond view -a ${v}.diamond_matches.daa -o ${v}.diamond_matches.m8
done




# 2b. Lambda
# build database
/sc/orga/work/buk02/Thirdparty/lambda-0.9.4-Linux-x86_64_sse4/bin/lambda_indexer -d all_complete_Gb_bac_protein.fasta

# run queries
SINGLE=/sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/*fastq
for r in $SINGLE
do
v=$(basename $r)
/sc/orga/work/buk02/Thirdparty/lambda-0.9.4-Linux-x86_64_sse4/bin/lambda -q $r -d all_complete_Gb_bac_protein.fasta
mv output.m8 ${v}.lambda_matches.m8
done

# 2c. Pauda

#relies on bowtie2
module load bowtie2

# create database
env PAUDA_INDEX_DIR=./index /sc/orga/work/buk02/Thirdparty/pauda-1.0.1/bin/pauda-build all_complete_Gb_bac_protein.fasta

# run queries
SINGLE=/sc/orga/projects/InfectiousDisease/cdiff_metagenomics/Raw/DNA.IlluminaHiSeq2500.WGS/Sample_CD01240/*fastq
for r in $SINGLE
do
# blastx fasta against protein db, do this in interactive1
v=$(basename $r)
/sc/orga/work/buk02/Thirdparty/pauda-1.0.1/bin/pauda-run $r ${v}.cdiff_pauda_matches.blastx ./index
# result in all_complete_Gb_bac_protein_cdiff_pauda1.blastx
done


# 3. MINERVA
# done in /sc/orga/scratch/buk02/6.13.16
vim ~/.bashrc # to edit

export PATH="$PATH:/sc/orga/work/buk02/minerva-queue-lsf/bin"

submitjob 24 -m 220 -c 1 -P acc_bashia02d -q alloc sh minervatest.sh



# 4. scp to mg-rast
# script for transfering spades assemblies to local drive
# ran in local terminal
SAMPLES=buk02@mothra.hpc.mssm.edu:/sc/orga/scratch/buk02/6.9.16/*
for s in $SAMPLES
do
f=$s/contigs.fasta
name=$(basename $s)
cp $f $f.$name.contigs.fasta
#scp $f kbpi31415@10.125.165.79:/users/kbpi31415/Desktop/ 
scp $f /Desktop

done



