import sys

import numpy as np
import matplotlib.mlab as mlab
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


gbk_fn = sys.argv[1]  # gbk database
reads_fn = sys.argv[2] # dict that maps  reads to genomes
uniq_reads_fn = sys.argv[3] # unique reads to be queried
output_fn = sys.argv[4] # desired output filename 

def build_feat_dict_from_gbk (gbkfile):
    from Bio import SeqIO
    feat_dict = {}
    for seq_record in SeqIO.parse(gbkfile, "genbank"):
        for feat in seq_record.features:
            if 'locus_tag' in feat.qualifiers:
                ltid = feat.qualifiers['locus_tag']
                feat_dict[ltid[0]] = (feat.location.start.position, feat.location.end.position, feat.location.strand)

    return feat_dict

def build_locus_dict_from_reads(readsfile):
	from Bio import SeqIO
	locus_dict = {}
	with open (readsfile) as f:
		for l in f:
			ll = l.strip().split()
			read_name = ll[0]
			locus_tag = ll[1].split('__')[1]
			locus_dict[read_name] = locus_tag  
	return locus_dict

feat_dict = build_feat_dict_from_gbk(gbk_fn)
locus_dict = build_locus_dict_from_reads(reads_fn)

#print locus_dict
#print feat_dict
positions = list()
with open (uniq_reads_fn) as f:
    for l in f:
        read = l.strip()
	locus_tag = locus_dict[read]
	# ll = l.strip().split()
        #locus_tag = ll[1].split('__')[1]
	try:
            positions.append(feat_dict[locus_tag][1])
        except:
            continue

#print positions
n, bins, patches = plt.hist(positions, 100000, normed=0, facecolor='green', alpha=0.75)

plt.xlabel('Position')
plt.xticks(rotation=270)
plt.ylabel('Frequency')
plt.title('Read position in genome')
#plt.axis([40, 160, 0, 0.03])
#plt.grid(True)
plt.savefig(output_fn + '.pdf', bbox_inches='tight')

