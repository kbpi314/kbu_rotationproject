import sys
gbkfile = sys.argv[1]

def build_feat_dict_from_gbk (gbkfile):
    from Bio import SeqIO
    feat_dict = {}
    for seq_record in SeqIO.parse(gbkfile, "genbank"):
        for feat in seq_record.features:
            if 'protein_id' in feat.qualifiers:
                pid = feat.qualifiers['protein_id']
                # assume exact positions known
                feat_dict[pid[0]] = (feat.location.start.position, feat.location.end.position, feat.location.strand)
    return feat_dict

build_feat_dict_from_gbk
