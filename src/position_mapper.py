
import sys
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt


gbk_fn = sys.argv[1]  # gbk database
reads_fn = sys.argv[2] # queried reads
output_fn = sys.argv[3] # desired output filename 

def build_feat_dict_from_gbk (gbkfile):
    from Bio import SeqIO
    feat_dict = {}
    for seq_record in SeqIO.parse(gbkfile, "genbank"):
        for feat in seq_record.features:
            if 'locus_tag' in feat.qualifiers:
                ltid = feat.qualifiers['locus_tag']
                feat_dict[ltid[0]] = (feat.location.start.position, feat.location.end.position, feat.location.strand)
           
    return feat_dict

feat_dict = build_feat_dict_from_gbk(gbk_fn)

positions = list()
with open (reads_fn) as f:
    for l in f:
        ll = l.strip().split()
        locus_tag = ll[-1].split('__')[1]
        try:
            positions.append(feat_dict[locus_tag][1])
        except:
            continue

n, bins, patches = plt.hist(positions, 10000, normed=0, facecolor='green', alpha=0.75)

plt.xlabel('Position')
plt.xticks(rotation=270)
plt.ylabel('Frequency')
plt.title('Read position in genome') 
#plt.axis([40, 160, 0, 0.03])
#plt.grid(True)
plt.savefig(output_fn + '.pdf', bbox_inches='tight')
#plt.show()



