# get_genbank_genomes.sh
# parse genbank for all complete genomes and create a single database file

#1. Get the list of assemblies:
FILE=assembly_summary.txt
if [ -f $FILE ]; then echo 'File $FILE found'; else wget ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/assembly_summary.txt; fi

#2. Parse the addresses of complete genomes from it
FILE2=assembly_summary_addresses_complete_genomes.txt
if [ -f $FILE2 ];
then echo 'File $FILE2 found';
else
awk -F '\t' '{if($12=="Complete Genome") print $20}' assembly_summary.txt > assembly_summary_addresses_complete_genomes.txt;
fi

#3. Make a dir for data
mkdir GbBacGenomes

#4. Fetch data
for next in $(cat assembly_summary_addresses_complete_genomes.txt); do name=$(basename $next); wget -P GbBacGenomes "$next"/${name}_genomic.fna.gz; done

#5. Extract data
gunzip GbBacGenomes/*.gz

#6. Concatenate data
cat GbBacGenomes/*.fna > all_complete_Gb_bac_genomes.fasta
