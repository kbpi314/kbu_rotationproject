# get_genbank_proteins_cdiff
# parse genbank for the set of proteins corresponding to cdiff genomes

#1. Get the list of assemblies:
FILE=assembly_summary.txt
if [ -f $FILE ]; then echo 'File $FILE found'; else wget ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/bacteria/assembly_summary.txt; fi

#2. Parse the addresses of complete cdiff genomes
FILE2=assembly_summary_cdiff.txt
if [ -f $FILE2 ];
then echo 'File $FILE2 found';
else
grep "difficile\|^#" assembly_summary.txt | grep "Complete Genome\|^#" > assembly_summary_cdiff.txt;
fi

#3. Write to summary file
FILE3=assembly_summary_addresses_complete_genomes_cdiff.txt
if [ -f $FILE3 ];
then echo 'File $FILE3 found';
else
awk -F '\t' '{if($12=="Complete Genome") print $20}' assembly_summary_cdiff.txt > assembly_summary_addresses_complete_genomes_cdiff.txt;
fi

#4. Make a dir for data
mkdir GbBacProteinsCdiff

#5. Fetch data
for next in $(cat assembly_summary_addresses_complete_genomes_cdiff.txt); do wget -P GbBacProteinsCdiff "$next"/*protein.faa.gz; done

#6. Extract data
gunzip GbBacProteinsCdiff/*.gz

#7. Concatenate data
cat GbBacProteinsCdiff/*.faa > all_complete_Gb_bac_proteins_cdiff.fasta  
