# file
R1=$1
R2=$2
REF=$3 
OUT=$4
# name of seq in file
REFNAME=$5 
# size of ref
REFLEN=$6
# step interval to plot coverage
STEP=$7
# get converage
module load bwa
module load samtools
module load BEDTools
module load python py_packages
# run BWA
bwa index $REF
bwa mem -t 12 $REF $R1 $R2 > $OUT.raw.sam

# filter SAM for full length all match alignments (maintain header)
awk '{if (substr($0, 0, 1) == "@" || $6 == "100M") print $0}' $OUT.raw.sam > $OUT.sam
 
# convert file to sorted bam
samtools view -Sbh $OUT.sam > $OUT.bam
samtools sort $OUT.bam $OUT.sorted
samtools index $OUT.sorted.bam

# bedtools coverage (deprecate)
#seq 1 $STEP $REFLEN | awk "{print $REFNAME , \$1 , \$1 + $STEP}" | sed 's/ /\t/g' > coords.bed
#bedtools coverage -abam $OUT.sorted.bam -b coords.bed
python /sc/orga/work/buk02/kbu_rotationproject/src/bamCoverage.py $OUT.sorted.bam $STEP 
